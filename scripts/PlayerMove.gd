extends CharacterBody2DSpace
signal player_dead

@export var acceleration: float = 2
@export var decrease: float = 2
@export var maxSpeed: float = 100
@export var rotationSpeed: float = 2

	
func _process(delta):
	if(Input.is_key_pressed(KEY_Z)):
		var localVelocity = velocity
		localVelocity += -transform.y * acceleration * delta
		velocity = localVelocity.limit_length(maxSpeed)
	elif (Input.is_key_pressed(KEY_S)):
		var localVelocity = velocity.lerp(Vector2.ZERO, decrease * delta)
		if(localVelocity.length() < 0.1):
			localVelocity = Vector2(0,0)
		velocity = localVelocity

	if(Input.is_key_pressed(KEY_Q)):
		rotation_degrees -= rotationSpeed;
	elif (Input.is_key_pressed(KEY_D)):
		rotation_degrees += rotationSpeed;
		
	move_and_collide(velocity)

func kill_me():
	player_dead.emit()
	queue_free()
