extends Node

var score: int = 0
var playerHP: int = 3

@onready var scoreLabel: Label = %Score
@onready var lifepointsLabels: Label = %LifePoint

@export var playerPrefab: PackedScene
@export var bigAsteroid: PackedScene
@export var mediumAsteroid: PackedScene
@export var smallAsteroid: PackedScene

var random: Random

# Called when the node enters the scene tree for the first time.
func _ready():
	scoreLabel.text = "Score : %s" % score
	lifepointsLabels.text = "Vies: %s" % playerHP
	random = Random.new()
	
	spawnAsteroid(bigAsteroid, 1, false, Vector2.ZERO)

func _input(event):
	if(event is InputEventKey && event.is_released() && event.keycode == KEY_R):
		get_tree().reload_current_scene()

func _on_vaisseau_joueur_player_dead():
	playerHP -= 1
	lifepointsLabels.text = "Vies: %s" % playerHP
	if(playerHP > 0):
		var player = playerPrefab.instantiate();
		player.connect("player_dead", _on_vaisseau_joueur_player_dead)
		call_deferred("add_child", player)

func _on_asteroid_destroyed(asteroidSize: AsteroidSizeEnum.ASTEROID_SIZE, newPosition: Vector2):
	# Par défaut, c'est un gros astéroide
	var addScore: int = 10
	var newChild: PackedScene = mediumAsteroid
	var nbChild: int = 3
	var spawnAtPos: bool = true
	
	match(asteroidSize):
		AsteroidSizeEnum.ASTEROID_SIZE.MEDIUM:
			addScore = 15
			newChild = smallAsteroid
			nbChild = 2
		AsteroidSizeEnum.ASTEROID_SIZE.SMALL:
			addScore = 50
			newChild = bigAsteroid
			nbChild = 1
			spawnAtPos = false
			
	score += addScore
	scoreLabel.text = "Score : %s" % score
	
	spawnAsteroid(newChild, nbChild, spawnAtPos, newPosition)

func spawnAsteroid(asteroidObject: PackedScene, nbChild: int, spawnAtPos: bool, newPosition: Vector2):
	for i in range(nbChild):
		var child = asteroidObject.instantiate() as AsteroidMove
		child.connect("asteroid_destroyed", _on_asteroid_destroyed)
		if(spawnAtPos):
			child.global_position = newPosition
		else:
			var width = get_viewport().get_visible_rect().size.x
			child.global_position = random.randv_circle(width, width + 10)
		call_deferred("add_child", child)
