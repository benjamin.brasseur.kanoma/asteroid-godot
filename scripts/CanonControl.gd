extends Node2D

@export var bulletPrefab: PackedScene
@export var cooldown: float = 0.1

var currentCooldown: float = 0.0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(currentCooldown > 0):
		currentCooldown -= delta
		return
	if(Input.is_key_pressed(KEY_SPACE)):
		var bullet = bulletPrefab.instantiate() as CharacterBody2DSpace
		bullet.global_position = global_position
		bullet.rotation = get_parent().rotation
		get_parent().get_parent().add_child(bullet)
		currentCooldown = cooldown
