extends CharacterBody2DSpace

@export var bulletSpeed: float = 5
var limitWidth: float
var limitHeight: float

# Called when the node enters the scene tree for the first time.
func _ready():
	velocity = -transform.y * bulletSpeed
	var viewport = get_viewport().get_visible_rect().size
	limitWidth = viewport.x
	limitHeight = viewport.y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(transform.origin.x > limitWidth || transform.origin.x < 0 
		|| transform.origin.y > limitHeight || transform.origin.y < 0):
		queue_free()
	var collision = move_and_collide(velocity)
	if(collision):
		var collider = collision.get_collider()
		if(collider is CharacterBody2DSpace && collider.get_collision_layer_value(3)):
			kill_me()
			collider.kill_me()
