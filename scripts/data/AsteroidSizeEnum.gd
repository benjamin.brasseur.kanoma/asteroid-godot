extends Node
class_name AsteroidSizeEnum

enum ASTEROID_SIZE {BIG, MEDIUM, SMALL}
