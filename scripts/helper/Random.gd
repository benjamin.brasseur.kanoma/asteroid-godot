extends RandomNumberGenerator
class_name Random

func randv_circle(min_radius := 1.0, max_radius := 1.0) -> Vector2:
	var r2_max := max_radius * max_radius
	var r2_min := min_radius * min_radius
	var r := sqrt(self.randf() * (r2_max - r2_min) + r2_min)
	var t := self.randf() * TAU
	return Vector2(r, 0).rotated(t)
