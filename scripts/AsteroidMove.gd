extends CharacterBody2DSpace
class_name AsteroidMove
signal asteroid_destroyed(asteroidSize: AsteroidSizeEnum.ASTEROID_SIZE, newPosition: Vector2)

@export var asteroidSize: AsteroidSizeEnum.ASTEROID_SIZE

var random: Random

# Called when the node enters the scene tree for the first time.
func _ready():
	random = Random.new()
	var direction = random.randv_circle(0.1, 1) * 2
	velocity = direction

func _physics_process(delta):
	super(delta)
	var collision = move_and_collide(velocity)
	if(collision):
		on_collision(collision.get_collider())

func on_collision(body: CharacterBody2DSpace):
	if(body.get_collision_layer_value(1)):
		body.kill_me()


func kill_me():
	asteroid_destroyed.emit(asteroidSize, global_position)
	super()
