extends Node

class_name HandlingOutside

var parent: CharacterBody2DSpace;
var maxWidth: float;
var maxHeight: float;

@export var warpMargin: float = 0.01;

func _ready():
	parent = get_parent()
	var viewport = get_viewport().get_visible_rect().size
	maxWidth = viewport.x;
	maxHeight = viewport.y;

func _process(_delta):
	var outOfWay = false;
	var newPos = parent.transform.get_origin()
	
	if (newPos.x - warpMargin > maxWidth):
		outOfWay = true;
		newPos.x = -warpMargin;
	elif (newPos.x + warpMargin < 0):
		outOfWay = true;
		newPos.x = maxWidth + warpMargin;

	if(newPos.y - warpMargin > maxHeight):
		outOfWay = true
		newPos.y = -warpMargin
	elif (newPos.y + warpMargin < 0):
		outOfWay = true
		newPos.y = maxHeight + warpMargin

	if (outOfWay):
		parent.teleport_object(newPos)
