extends CharacterBody2D

class_name CharacterBody2DSpace

var teleportObject: bool = false
var rotateObject: bool = false
var newPosition: Vector2
var newRotation: float

func teleport_object(aimedPosition: Vector2):
	newPosition = aimedPosition
	teleportObject = true

func rotate_object(aimedRotation: float):
	newRotation = aimedRotation
	rotateObject = true

func _physics_process(_delta):
	if(teleportObject):
		transform.origin = newPosition
		teleportObject = false

func kill_me():
	queue_free()
